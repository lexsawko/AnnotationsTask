package by.itcourses.validation;

import by.itcourses.SexEnum;

import java.lang.reflect.Field;

public class SexProcessor implements ValidationProcessor {
    public void process(Object obj, Field field) throws ValidationException{
        try {
            SexEnum sex =(SexEnum) field.get(obj);
            if(sex==null){
                throw new ValidationException("Пол не задан");
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
