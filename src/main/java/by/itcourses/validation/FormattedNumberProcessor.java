package by.itcourses.validation;

import by.itcourses.valiationAnnotations.FormattedNumber;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormattedNumberProcessor implements ValidationProcessor {
    public void process(Object obj, Field field) throws ValidationException{
        try {
            Integer min = field.getAnnotation(FormattedNumber.class).min();
            Integer max = field.getAnnotation(FormattedNumber.class).max();
            Integer value = (Integer) field.get(obj);

            if(value>max || value<min){
                throw new ValidationException("Значение поля " + field.getName() +
                        " должно быть в пределах от " + min + " до " + max);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
