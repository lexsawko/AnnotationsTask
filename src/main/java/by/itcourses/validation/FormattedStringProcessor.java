package by.itcourses.validation;

import by.itcourses.valiationAnnotations.FormattedString;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormattedStringProcessor implements ValidationProcessor {
    public void process(Object obj,Field field) throws ValidationException {
        try {
            String format = field.getAnnotation(FormattedString.class).format();
            String string = (String) field.get(obj);
            Pattern p = Pattern.compile(format);
            Matcher m = p.matcher(string);
            if(!m.matches()){
                throw new ValidationException("Неверный формат ввода в поле " + field.getName());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
