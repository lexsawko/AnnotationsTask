package by.itcourses.validation;

import by.itcourses.valiationAnnotations.FormattedNumber;
import by.itcourses.valiationAnnotations.FormattedString;
import by.itcourses.valiationAnnotations.Sex;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Validator {
    List<String> errorsList = new ArrayList<String>();

    public List<String> getErrors() {
        return errorsList;
    }

    public void validate(Object obj) {
        Field[] fields =  obj.getClass().getDeclaredFields();
        for(Field field: fields){
            field.setAccessible(true);
                try {
                    if(field.isAnnotationPresent(FormattedString.class)) {
                    new FormattedStringProcessor().process(obj,field);
                    }
                    if(field.isAnnotationPresent(FormattedNumber.class)) {
                        new FormattedNumberProcessor().process(obj,field);
                    }
                    if(field.isAnnotationPresent(Sex.class)) {
                        new SexProcessor().process(obj,field);
                    }
                }catch (ValidationException e) {
                    errorsList.add(e.getMessage());
                }
        }
    }
}
