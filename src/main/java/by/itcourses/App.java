package by.itcourses;

import by.itcourses.validation.Validator;

public class App {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Aleksandr");
        person.setSurname("Sawko");
        person.setAge(20);
        person.setHeight(182);
        person.setWeight(200);
        person.setSex(SexEnum.MALE);
        person.setPhone("+375259316620");
        person.setBirthday("1997-04-22");
        Validator validator = new Validator();
            validator.validate(person);
        System.out.println("Ошибки: " + validator.getErrors());
    }
}
