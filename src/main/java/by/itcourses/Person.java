package by.itcourses;

import by.itcourses.valiationAnnotations.FormattedNumber;
import by.itcourses.valiationAnnotations.FormattedString;
import by.itcourses.valiationAnnotations.Sex;

public class Person {
    @FormattedString(format = "[a-zA-Zа-яА-ЯёЁ]{2,16}\\-?[a-zA-Zа-яА-ЯёЁ]{2,16}")
    private String name;
    @FormattedString(format = "[a-zA-Zа-яА-ЯёЁ]{2,16}\\-?[a-zA-Zа-яА-ЯёЁ]{2,16}")
    private String surname;
    @FormattedNumber //default max=120, min=0
    private Integer age;
    @FormattedNumber(max=220, min=100) //default
    private Integer height;
    @FormattedNumber (max= 200, min= 40) //default
    private Integer weight;
    @Sex
    private SexEnum sex;
    @FormattedString(format = "(80|\\+375)(25|29|33)[0-9]{7}")
    private String phone;
    @FormattedString(format="(19|20)\\d\\d-((0[1-9]|1[012])-(0[1-9]|[12]\\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)")
    private String birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
